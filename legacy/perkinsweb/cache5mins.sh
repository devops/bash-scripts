#!/bin/bash

# Determine cache hit ratio
# This could probably be a 1-liner with sed or something
#
# -jnt


# First 3 results are derived from log scraping
# return hits, misses, and ratio for last 5 mins
HITS=`cat /var/log/httpd/newhits`
MISSES=`cat /var/log/httpd/newmisses`

echo "${HITS}"
echo "${MISSES}"

RATIO=`echo "100*$HITS/($HITS+$MISSES)"|bc`
echo "${RATIO}"

# Next 3 results come from the cache clear log
# return total current entries, total previous entries, total pruned entries

ENTRIES=`tail -1 /tmp/cacheclean.log | cut -f 8 -d \ `
OLDENTRIES=`tail -1 /tmp/cacheclean.log | cut -f 4 -d \ | perl -pe 's/.$//'`

echo "${ENTRIES}"
echo "${OLDENTRIES}"
NUMPRUNED=`echo "${OLDENTRIES} - ${ENTRIES}"|bc`
echo "${NUMPRUNED}"
