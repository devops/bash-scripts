#!/bin/env python
import re
import sys
import os.path
import simplejson

logfile = "/var/log/httpd/library_cache_log"
#logfile = "intranet_cache_log"
statfile = "/var/log/httpd/cachestats.json"
#statfile = "statfile.json"

def readjson(file):
  try:
    io = open(file)
    dict = simplejson.load(io)
    io.close()
    if not 'hittotal' in dict:
      dict['hittotal'] = 0
    if not 'misstotal' in dict:
      dict['misstotal'] = 0
    if not 'hitbytes' in dict:
      dict['hitbytes'] = 0
    if not 'missbytes' in dict:
      dict['missbytes'] = 0
  # It's quite possible for the file to not exist, so prime the dictionary values with 0 if so
  except:
    dict = {
      'hittotal': 0,
      'misstotal': 0,
      'hitbytes': 0,
      'missbytes': 0,
    }  
  return dict

def writejson(file, dict):
  io = open(file, "w")
  dict = simplejson.dump(dict, io)
  io.close()

def clearlog(file):
  io = open(file, "w")
  io.close()

def getoldhits(file):
  numhits = 0
  io = open(file)
  for line in io:
    numhits = int(line)
  io.close()
  return numhits

# Reads a log file into a list of lists
def readlog(file):
  io = open(file)
  results = list()
  for entry in io:
    li = entry.split()
    #print li[-3] + li[-2] + li[-1]
    results.append([ li[-3], li[-2], li[-1] ])
  io.close()
  return results

# 88.25.112.63 [15/Aug/2008:16:25:05 -0400] /digitalcollections/app/css/digcoll.css 200 13729 3922
def main():
  logresults = readlog(logfile)
  #print logresults

  misstotal = 0
  hittotal = 0
  hitbytes = 0
  missbytes = 0
  
  for result in logresults:
    # misses end in -
    if result[2] == '-':
      misstotal += 1
      # For some reason, it's possible to get - for size
      if result[1] == '-':
        missbytes += 0
      else:
        missbytes += int(result[1])
    # hits end in anything other than -
    else:
      hittotal += 1
      # For some reason, it's possible to get - for size
      if result[1] == '-':
        hitbytes += 0
      else:
        hitbytes += int(result[1])
      #print "Hit!" + result[2]


#'Matching on regex: "%s"' % FILTER_RE
  print "Total from log: %s hits, %s bytes" % (hittotal, hitbytes)
  print "Total from log: %s misses, %s bytes" % (misstotal, missbytes)
  olddata = readjson(statfile)
  newdata = {
    'runhittotal': hittotal,
    'runhitbytes': hitbytes,
    'runmisstotal': misstotal,
    'runmissbytes': missbytes,
    'hittotal': olddata['hittotal'] + hittotal,
    'misstotal': olddata['misstotal'] + misstotal,
    'missbytes': olddata['missbytes'] + missbytes,
    'hitbytes': olddata['hitbytes'] + hitbytes
  }
  print "Total prior: %s hits, %s bytes" % (olddata['hittotal'], olddata['hitbytes'])
  print "Total prior: %s misses, %s bytes" % (olddata['misstotal'], olddata['missbytes'])
  print "New Total: %s hits, %s bytes" % (newdata['hittotal'], newdata['hitbytes'])
  print "New Total: %s misses, %s bytes" % (newdata['misstotal'], newdata['missbytes'])
  writejson(statfile, newdata)
  clearlog(logfile)

if __name__ == '__main__':
  main()
