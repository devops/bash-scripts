#!/bin/env python
import simplejson

statfile = "/var/log/httpd/cachestats.json"
cleanlog = "/tmp/cacheclean.log"

'''
# Next 3 results come from the cache clear log
# return total current entries, total previous entries, total pruned entries

ENTRIES=`tail -1 /tmp/cacheclean.log | cut -f 8 -d \ `
OLDENTRIES=`tail -1 /tmp/cacheclean.log | cut -f 4 -d \ | perl -pe 's/.$//'`

echo "${ENTRIES}"
echo "${OLDENTRIES}"
NUMPRUNED=`echo "${OLDENTRIES} - ${ENTRIES}"|bc`
echo "${NUMPRUNED}"
size limit 1024.0M
total size was 218.5M, total size now 218.5M
total entries was 7611, total entries now 7611
[['Statistics:'], ['size', 'limit', '1024.0M'], ['total', 'size', 'was', '218.5M,', 'total', 'size', 'now', '218.5M'], ['total', 'entries', 'was', '7611,', 'total', 'entries', 'now', '7611']]
'''

def readjson(file):
#  dict = {}
  try:
    io = open(file)
    dict = simplejson.load(io)
    io.close()
    if not 'hittotal' in dict:
      dict['hittotal'] = 0
    if not 'misstotal' in dict:
      dict['misstotal'] = 0
    if not 'hitbytes' in dict:
      dict['hitbytes'] = 0
    if not 'missbytes' in dict:
      dict['missbytes'] = 0

  except:
    dict = {
      'hittotal': 0,
      'misstotal': 0,
      'hitbytes': 0,
      'missbytes': 0,
    }  
  return dict

def readcleanlog(file):
  log = list()
  try:
    io = open(file)
    for entry in io:
      li = entry.split()
      if li[0] != 'unsolicited':
        log.append(li)
    io.close()
    results = {
      'limit': log[1][2].strip("M,"),
      'oldsize': log[2][3].strip("M,"),
      'newsize': log[2][7].strip("M,"),
      'oldnum': int(log[3][3].strip("M,")),
      'newnum': int(log[3][7].strip("M,"))
    }
  except:
    results = {
      'limit': 0,
      'oldsize': 0,
      'newsize': 0,
      'oldnum': 0,
      'newnum': 0
    }
    
  return results
    
def main():
  olddata = readjson(statfile)
  cleandata = readcleanlog(cleanlog)
  # 1 - hits from last log parse
  print str(olddata['runhittotal'])
  # 2 - misses from last log parse
  print str(olddata['runmisstotal'])
  # 3 - ratio from last log parse
  try:
    print str((100*olddata['runhittotal'])/(olddata['runhittotal'] + olddata['runmisstotal']))
  except:
    print 0
  # 4 - total number of entries in cache after cache clean
  print str(cleandata['newnum'])
  # 5 - total number of entries in cache prior to cache clean
  print str(cleandata['oldnum'])
  # 6 - total number of cache entries removed during clean
  print str((cleandata['oldnum'] - cleandata['newnum']))
  # 7 - size of all cache hits from the last log parse
  print olddata['runhitbytes']
  # 8 - size of all cache misses from the last log parse
  print olddata['runmissbytes']
  # 9 - ratio of hit size to miss size from last log parse
  try:
    print (100*olddata['runhitbytes'])/(olddata['runhitbytes'] + olddata['runmissbytes'])
  except:
    print 0

# return total current entries, total previous entries, total pruned entries


if __name__ == '__main__':
    main()
