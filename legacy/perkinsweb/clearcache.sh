#!/bin/bash

if [ $USER != "apache" ] ; then
  echo "ERROR - must be run as apache user (sudo -u apache $0)"
  exit 2
fi

echo "Processing..."
/usr/sbin/htcacheclean -v -p/cache/mod_proxy/ -l1
