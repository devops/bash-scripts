#!/bin/bash

find /var/www/pdf -type f -name '*.pdf' -mtime +1644 -exec mv \{\} /var/www/pdf/archive/ \; -o -name 'archive*' -prune
#find /var/www/pdf -type f -name '*.pdf' -mtime +1068 -exec ls -ld \{\} \; -o -name 'archive*' -prune

