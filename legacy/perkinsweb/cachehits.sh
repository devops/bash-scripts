#!/bin/bash

# Determine cache hit ratio
# This could probably be a 1-liner with sed or something
#
# -jnt

if ! test -z $1 ; then
	LINES=$1
else
	LINES=1000
	echo "Using default ${LINES} lines - you may specify a value with ${0} [numlines]"
fi

HITS=`tail -n $LINES /var/log/httpd/library_cache_log | grep "[0-9]$" | wc -l` 
MISSES=`tail -n $LINES /var/log/httpd/library_cache_log | grep -v "[0-9]$" | wc -l`
TOTAL=`echo "$HITS+$MISSES"|bc`
RATIO=`echo "scale=2;100*$HITS/($HITS+$MISSES)"|bc`
echo ""
echo "Stats for the last $TOTAL hits:" 
echo ""
echo "Hits: $HITS"
echo "Misses: $MISSES" 
echo "Hit Ratio: ${RATIO}%"
echo ""
