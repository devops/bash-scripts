#!/bin/bash

# Move the most recent apache logs to sawmill


SAWMILLHOST=sawmill.lib.duke.edu
SAWMILLDIR=/opt/logfiles/www/apache/

# Only get the most recent logs
LOGFILES=`find /var/log/httpd/ -name '*.gz' -mtime -7 ! -name '*click*' ! -name 'library_cache_log*' ! -name 'acess_log*' ! -name 'error_log*'\;`

for LOG in ${LOGFILES}; do
	BASENAME=`echo ${LOG} | perl -ne 'm|/var/log/httpd/(.*?)_.*| && print $1;'`
	scp ${LOG} sawmill@${SAWMILLHOST}:${SAWMILLDIR}logs_${BASENAME}/
done

# special job for click logs
find /var/log/httpd/ -name '*click*gz' -mtime -7 -exec scp '{}' sawmill@${SAWMILLHOST}:${SAWMILLDIR}clicklogs/ \;
