#!/usr/bin/env python

''' Update multiple blogs from a single wordpress distribution '''

import sys
import os
import subprocess
from optparse import OptionParser

plugindir = "/wp-content/plugins"
wpsource = "/usr/local/wordpress"
rsyncopts = "-raz"
tarballdir = "/tmp/"
productionblogs = {
    'scholcomm': "/var/www/library/blogs/scholcomm",
    'libraryhacks': "/var/www/library/blogs/libraryhacks",
    'music': "/var/www/library/blogs/music",
    'answerperson': "/var/www/library/blogs/answerperson",
    'techmentor': "/var/www/library/blogs/techmentor",
    'magazine': "/var/www/library/magazine",
    'oleproject': "/var/www/library/blogs/oleproject",
    'digital-collections': "/var/www/library/blogs/digital-collections",
    'spotlight': "/var/www/library/blogs/divinity-spotlight",
    'citnews': "/var/www/cit/blogs/citnews",
    'citprofiles': "/var/www/cit/blogs/citprofiles",
    'imlts': "/var/www/cit/blogs/imlts_profiles",
    'showcase2008': "/var/www/cit/blogs/showcase2008",
    'showcase2009': "/var/www/cit/blogs/showcase2009"
}
    
developmentblogs = {
    'humanrights': "/var/www/library/blogs/humanrights"
}

intranetblogs = {
    'staff': "/var/www/html/blogs/staff",
    'citstaff': "/var/www/html/blogs/citstaff"
}

allblogs = {}
allblogs.update(productionblogs)
allblogs.update(developmentblogs)
allblogs.update(intranetblogs)

def main():
    """ Main program execution """
    if len(sys.argv) < 2:
        print "please specify '-h' for usage help"
        sys.exit()
    (options, args) = parse_opts()
    if options.list:
        list_blogs()
        sys.exit()
    if options.tarball:
        if options.tarball in allblogs:
            make_tarball(allblogs[options.tarball])
        sys.exit()
    if options.blog:
        if options.blog in allblogs:
            update_blog(allblogs[options.blog],wpsource,options.noask)
        sys.exit()
    if options.production:
        print "UPDATING ALL PRODUCTION BLOGS"
        for k,v in productionblogs.items():
            update_blog(v,wpsource,options.noask)
        sys.exit()
    if options.development:
        print "UPDATING ALL DEVELOPMENT BLOGS"
        for k,v in developmentblogs.items():
            update_blog(v,wpsource,options.noask)
        sys.exit()
    if options.intranet:
        print "UPDATING ALL INTRANET BLOGS"
        for k,v in intranetblogs.items():
            update_blog(v,wpsource,options.noask)
        sys.exit()



def update_blog(target, source=wpsource, noask=False):
    """
    Creates a backup tarball of target blog and then updates from source. By default, asks confirmation before doing so.
    """
    print "-------"
    if os.path.isdir(target):
        print "Found blog: " + target
        if os.path.isdir(target + plugindir):
            print "Note - the following files exist in the plugins directory and will be obliterated if you proceed:"
            print "\n",
            for plugin in os.listdir(target + plugindir):
                print plugin,
            print "\n"
        if noask or user_confirm("Install in this directory?"):
            # tar -C ~/ -czvf /tmp/puppet.tar.gz puppet
            if not make_tarball(target, tarballdir):
                print "ERROR - unable to create tarball! Bailing out!"
                return False
            print "Syncing wordpress installation..."
            if subprocess.call(["rsync", rsyncopts, source + "/", target]):
                print "ERROR - rsync failed! Check permissions?"
                return False
            print "Purging old plugins..."
            subprocess.call(["rsync", rsyncopts, "--delete", source + plugindir + "/", target + plugindir])
        else:
            print "Skipping on user input...\n"
    else:
        print "NOT FOUND: " + target
        print "ERROR - this blog is defined, but the directory does not exist on this server!"
        print "If you'd like to install a new blog in this location, please create an empty"
        print "directory in this location first:" + target + "\n"
    print "-------"
    return True

def make_tarball(target, tarballdir="/tmp/"):
    """
    Creates a tarball of target directory as tarballdir/target.tar.gz
    """
    print "Creating tarball in %s ..." % (tarballdir + os.path.split(target)[1] + ".tar.gz")
    if subprocess.call(["tar", "-C", os.path.split(target)[0], "-czf", tarballdir + os.path.split(target)[1] + ".tar.gz", os.path.split(target)[1]]):
        return False
    return True

def user_confirm(text="Proceed?"):
    """
    Displays text and ~Requests user confirmation - returns True if user inputs 'y', False for all other input.
    """
    print text
    answer = raw_input("(y to continue, everything else aborts): ")
    if answer != 'y':
        return False
    else:
        return True
    
def list_blogs():
    """
    Lists all defined blogs.
    """
    print "Listing all blogs\n\n"
    print "Production blogs:"
    for k,v in productionblogs.items():
        print "\t%s: %s" % (k,v)
    print "\nDevelopment blogs:"
    for k,v in developmentblogs.items():
        print "\t%s: %s" % (k,v)
    print "\nIntranet blogs:"
    for k,v in intranetblogs.items():
        print "\t%s: %s" % (k,v)

def parse_opts():
    """
    Parses command line options.
    """
    parser = OptionParser(usage="Blog updating script - rsyncs the source wordpress directory at %s to multiple other installations.\nusage: %%prog [options]" % wpsource)
    parser.add_option("-b", "--blog", help = "update an individual named blog (corresponds with names in -l)", dest = "blog")
    parser.add_option("-l", "--list", help = "list all blogs and their locations", action = "store_true")
    parser.add_option("-t", "--tarball", help = "generate tarball of specified blog and exitl", dest = "tarball", metavar = "BLOG")
    parser.add_option("-n", "--noask", help = "disable verification questions (dangerous!)", action = "store_true")
    parser.add_option("-i", "--intranet", help = "update ALL intranet blogs", action = "store_true")
    parser.add_option("-d", "--development", help = "update ALL development blogs", action = "store_true")
    parser.add_option("-p", "--production", help = "update ALL production blogs", action = "store_true")
    return parser.parse_args()


if __name__ == '__main__':
    main()
