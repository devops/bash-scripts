#!/bin/sh
#
# The purpose of this script is to find and delete empty directories
# under the CMS publishing root directory.  When the CMS "unpublishes"
# content, it does not remove directories, so we want to clean these
# up periodically.  The list of matching directories, if not empty,
# is mailed to the DPD box for their information.
#
# Written by: David Chandek-Stark, 04/11/2011.
#
cmsdir="/var/www/dukeweb"
mailsubj="Empty CMS-published directories deleted"
mailto="library-digital-projects@duke.edu"
tempfile=`mktemp`
find ${cmsdir} -type d -empty -fprint ${tempfile} -delete
if [ -s ${tempfile} ]; then
    sort ${tempfile} | mail -s "${mailsubj}" ${mailto}
fi
