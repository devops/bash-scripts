#!/bin/bash

# Maintain hits and misses based on the cache log
#
# This should be run every 5 minutes

# -jnt

HITFILE=/var/log/httpd/cachehits
MISSFILE=/var/log/httpd/cachemisses

NEWHITFILE=/var/log/httpd/newhits
NEWMISSFILE=/var/log/httpd/newmisses

OLDHITS=0`[ -f ${HITFILE} ] && cat ${HITFILE}` 
OLDMISSES=0`[ -f ${MISSFILE} ] && cat ${MISSFILE}`

NEWHITS=`grep "[0-9]$" /var/log/httpd/library_cache_log | wc -l` 
NEWMISSES=`grep -v "[0-9]$" /var/log/httpd/library_cache_log | wc -l` 

echo "${NEWHITS}+${OLDHITS}" | bc > ${HITFILE}
echo "${NEWMISSES}+${OLDMISSES}" | bc > ${MISSFILE}

echo ${NEWHITS} > ${NEWHITFILE}
echo ${NEWMISSES} > ${NEWMISSFILE}

cat /dev/null > /var/log/httpd/library_cache_log
