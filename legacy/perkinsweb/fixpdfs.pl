#!/usr/bin/perl -w

######
# Touch any PDF files from the list of active course materials
# -jnt 8/15/07
######

# If you have a bunch of lists of files, you could do something like:
# for pdflist in pdflists/*; do ./fixpdfs.pl -f $pdflist -p /WWW/pdf/archive/; done

use strict;
use Getopt::Std;
use Time::Local;

# Push command line options to a hash
my %options;
my $arg = getopts('f:p:d:', \%options);
if ((!$arg) || ($options{'?'}) || (!%options) || (!(($options{'f'}) && ($options{'p'}) ))) {
        print "Examine course files and time stamp their based on last use\n";
        print "  Usage: $0 -f <list of files> -p <path of files> [-d <date>]\n";
	print "    Where:\n";
        print "    <list of files> is a file containing line-delimited list of PDF files\n";
        print "    <path of files> is the directory which contains files to be examined\n";
        print "    <date> is the datestamp to add.  This is in format YYYYMMDD (e.g. 20041021).\n";
        print "           If ommited, uses end of filename.  This format is like \"date +\"\%Y\%m\%d\"\" \n\n";
	print " WARING - this script doesn't verify input at all.  If you put in something stupid, IT WILL \n";
	print " DO BAD THINGS!!\n";
        exit (3);
}


# Where our master list resides
#my $filelist="/u/jthornhi/eres-active-pdf-list.txt";
my $filelist="$options{'f'}";

# Where we're looking for files
#my $directory="/u/jthornhi/pdf/";
my $directory="$options{'p'}";

# Looks like "20041215"
my $inputdate="$filelist";
$inputdate="$options{'d'}" if $options{'d'};

# No error checking - better hope the user knows what they're doing
$inputdate =~ m/.*(\d{4})(\d{2})(\d{2})$/;
my $day = $3;
my $month = $2;
my $year = $1;
my $date = timelocal(0, 0, 0, $day, $month-1, $year-1900);

my %lookfiles;
my $filename;
#my $date = time;

my @havefiles = list_files();

# Using a hash strips out non-unique values
foreach $filename (cat_files()){
	$lookfiles{$filename}=();
}	

# Iterate through the file list and touch files
foreach (keys %lookfiles) {
	my $string = $_;
	# Is there a faster way to do this?  Mixed case makes stuff hard.
#	my @match = join("",grep(/^$string/i, @havefiles));
	my @match = grep(/^$string/i, @havefiles);
	if ($match[0]) {
		print "TOUCHING ${month}/${day}/${year} (epoch ${date}):  $match[0] - matches $string\n";
		touch_file("$match[0]");
	}
	else {
#		print "WARNING: $string not found in any files\n";
	}
}

# return a list of files in the directory
sub list_files {
	opendir(DIR, $directory) || die "can't opendir $directory: $!";
	my @list_files = readdir(DIR);
	closedir DIR;
	return @list_files;
}

# return a list of files in the text file
sub cat_files {
	open FILELIST, $filelist or die "Unable to open $filelist";
	my @cat_files;
	while (<FILELIST>) {
		chomp;
# Try to eliminate problems with spaces
		s/\ //;
		push (@cat_files,"$_");
	}
	close FILELIST;
	return @cat_files;
}

# Touch files
sub touch_file {
	my $file = $directory.$_[0];
	# Verify that the file exists and isn't null - this shouldn't really ever happen
	if ( -s $file ) {
#		print "Would update $file with date $date \n";
		utime $date, $date, $file || print STDERR "ERROR: can't update file $file $!\n";
	}
	else {
		print STDERR "ERROR: $file matched pattern but disappeared or is null?\n";
	}
}


