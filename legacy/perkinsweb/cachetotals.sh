#!/bin/bash

# Determine cache hit ratio
# This could probably be a 1-liner with sed or something
#
# -jnt

HITS=`cat /var/log/httpd/cachehits`
MISSES=`cat /var/log/httpd/cachemisses`

echo "${HITS}"
echo "${MISSES}"
RATIO=`echo "100*$HITS/($HITS+$MISSES)"|bc`
echo "${RATIO}"
#cat /dev/null > /var/log/httpd/library_cache_log
