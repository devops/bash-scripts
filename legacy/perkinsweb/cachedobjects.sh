#!/bin/bash

# Get the number of cached objects from the cache clear log

ENTRIES=`tail -1 /tmp/cacheclean.log | cut -f 8 -d \ `
OLDENTRIES=`tail -1 /tmp/cacheclean.log | cut -f 4 -d \ | perl -pe 's/.$//'`

echo "${ENTRIES}"
echo "${OLDENTRIES}"
NUMPRUNED=`echo "${OLDENTRIES} - ${ENTRIES}"|bc`
echo "${NUMPRUNED}"
